const input = document.querySelector("input[name='fruit']")

input.onkeyup = (event) => {
    if (event.key === "Enter") {
        const li = document.createElement("li");
        li.innerText = input.value;
        document.querySelector(".fruit-list").append(li)
    }
}