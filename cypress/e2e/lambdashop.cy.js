/// <reference types="cypress" />

beforeEach(() => {
    // Använder baseUrl
    cy.visit("/")
})

// SKapar eget, återanvändningsbart kommmando för inloggning
Cypress.Commands.add("login", (email, password) => {

    const formData = new FormData();
    formData.set("email", email);
    formData.set("password", password)

    cy.request({
        url: "https://ecommerce-playground.lambdatest.io/index.php?route=account/login",
        method: 'POST',
        body: formData,
        headers: {
            'Content-Type': "multipart/form-data;"
        },
        failOnStatusCode: true
    })
        .then(() => cy.reload())

    // Verifiera inloggning via returnerad cookie
    cy.getCookie("OCSESSID").should('exist')

    // Verifierar via gränssnittet att vi är inloggade
    cy.get('#widget-navbar-217834 > .navbar-nav > :nth-child(6) > .nav-link')
        .trigger("mouseover")
    cy.contains("Dashboard").should('be.visible')

})

describe("Shopping", () => {

    const email = "gunnar@gunnar.nu";
    const password = 1234

    it("it should be able to login and buy a product", () => {

        // Triggar event för att motsvara att vi hovrar för att få fram dropdown-menyn
        cy.get('#widget-navbar-217834 > .navbar-nav > :nth-child(6) > .nav-link')
            .trigger("mouseover")

        /*  cy.contains('Login').click()
 
         cy.get('#input-email').type(email)
         cy.get('#input-password').type(password)
 
         cy.get('form > .btn').click()
          */

        // Eget command - Se Cypress.Commands.Add ovan
        cy.login(email, password)

    })

})