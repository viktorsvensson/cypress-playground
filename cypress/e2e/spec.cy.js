/// <reference types="cypress"/>

describe("Queries", () => {

  beforeEach(() => {
    cy.viewport(400, 400)
    cy.visit("http://localhost:5500")
  })

  it('Should find our title', () => {

    // Hitta element genom elementtagg
    cy.get("h1")

    // Hitta element genom class
    cy.get(".header")

    // Genom id
    cy.get("#title")

    // Genom cypress rekommenderade data-test-id
    cy.get("[data-test-id='title']")

    // Genom css-Selectorer
    cy.get(".fruit-list > li:last")

    // Genom förslag från cypress på Selector
    cy.get('.fruit-list > :nth-child(3)')

  })

  it('Should find an element containing some text', () => {

    cy.contains("Apelsin")

    cy.contains(/an/)

    cy.get("li:contains(i)")

    cy.get(".fruit-list").contains(/i/)

  })

  it("Should traverse our elements", () => {

    // Find - hittar element i det queriade elementet
    cy.get(".fruit-list").find("li")

    // first - hittar det första elementet i en lista av element
    cy.get(".fruit-list").find("li").first()

    // next - hittar nästa sibling-element
    cy.get(".fruit-list").find("li").first().next()

    // nextAll - hittar alla efter det element man stod på
    cy.get(".fruit-list").find("li").first().nextAll()

    // nextUntil - hittar alla elementer efter det man stod på till något visst villkor
    cy.get(".fruit-list").find("li").first().nextUntil(":contains(E)")

    // eq - hittar ett element på ett specifikt index 
    cy.get(".fruit-list").find("li").eq(2)

  })

})

describe("Actions", () => {

  beforeEach(() => {
    cy.viewport(400, 400)
    cy.visit("http://localhost:5500")
  })

  it("Should write in something", () => {

    cy.get("input[name='fruit']").type("Drakfrukt")

    cy.get("input[name='fruit']").clear()

    cy.get("input[type='date']").type("2024-04-19")

    cy.get("input[name='fruit']").type("Drakfrukt{enter}", {
      delay: 10
    })

  })

  it("Should click something", () => {

    cy.get("button").click()

    cy.get("[type='checkbox']").check()

    cy.get("[type='checkbox']").uncheck()

  })

  it("Should select some option", () => {

    cy.get('select').select("Tiger")

  })

})

describe("Assertions", () => {

  beforeEach(() => {
    cy.viewport(400, 400)
    cy.visit("http://localhost:5500")
  })

  it("Should add new fruits to the bottom of the list", () => {

    let text = "Drakfrukt"

    cy.get("input[name='fruit']")
      .type(text)
      .type("{enter}")
      .get(".fruit-list")
      .find("li")
      .should("have.length", 5)
      .should("contain.text", text)

    cy.get(".fruit-list")
      .find("li")
      .last()
      .invoke('text')
      .then(text => {
        text = text.replace(/\D/g, "")
        cy.log(text)
        return cy.wrap(+text)
      })
      .should('eq', 10)

  })

})